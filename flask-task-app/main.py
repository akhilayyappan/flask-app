from flask import Flask, request, jsonify, abort
from flask_sqlalchemy import SQLAlchemy
import uuid
import os
app = Flask(__name__)
db_path = os.path.join(os.path.dirname(__file__), 'app.db')
db_uri = 'sqlite:///{}'.format(db_path)
app.config[
    'SQLALCHEMY_DATABASE_URI'] = db_uri
db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(50), unique=True)
    name = db.Column(db.String(50))
    email_id = db.Column(db.String(50))
    mobile_number = db.Column(db.Integer)


@app.route('/')
def home():
    return "WELCOME"


@app.route('/users/<public_id>', methods=['GET'])
def get_single_user(public_id):
    user = User.query.filter_by(public_id=public_id).first()
    if not user:
        return jsonify({'message': 'no user found', 'result': 'failure'}), 404
    user_data = {}
    user_data['name'] = user.name
    user_data['email_id'] = user.email_id
    user_data['mobile_number'] = user.mobile_number
    user_data['public_id'] = user.public_id
    return jsonify({'user': user_data})


@app.route('/users', methods=['GET'])
def get_all_users():
    return jsonify(get_paginated_list(
        User,
        '/users',
        offset=request.args.get('offset', 1),
        limit=request.args.get('limit', 10)
    ))


def get_paginated_list(klass, url, offset, limit):
    # check if page exists
    users = klass.query.all()
    obj = {}
    if not users:
        obj['message'] = 'No User data found'
        obj['users'] = '[]'
    else:
        results = []
        offset = int(offset)
        limit = int(limit)
        for user in users:
            user_data = {}
            user_data['name'] = user.name
            user_data['email_id'] = user.email_id
            user_data['mobile_number'] = user.mobile_number
            user_data['public_id'] = user.public_id
            results.append(user_data)
        count = len(results)
        if (count < offset):
            abort(404)
        # make response
        if limit >= 20:
            limit = 10
        obj['offset'] = offset
        obj['limit'] = limit
        obj['count'] = count
        # make URLs
        # make previous url

        if offset == 1:
            obj['previous'] = ''
        else:

            start_copy = max(1, offset - limit)
            limit_copy = offset - 1
            obj['previous'] = url + '?offset=%d&limit=%d' % (start_copy, limit_copy)
        # make next url
        if offset + limit > count:
            obj['next'] = ''
        else:
            start_copy = offset + limit
            obj['next'] = url + '?offset=%d&limit=%d' % (start_copy, limit)
        # finally extract result according to bounds
        obj['results'] = results[(offset - 1):(offset - 1 + limit)]
    return obj


@app.route('/users/create', methods=['POST'])
def create_user():
    data = request.get_json()
    email_id = data['email_id']
    mobile_number = data['mobile_number']
    duplicate_email = User.query.filter_by(email_id=email_id).first()
    duplicate_mobile_number = User.query.filter_by(mobile_number=mobile_number).first()
    if duplicate_email or duplicate_mobile_number:
        return jsonify({'message': 'user has already exist', 'result': 'failure'}), 409
    else:
        new_user = User(public_id=str(uuid.uuid4()), name=data['name'], email_id=data['email_id'],
                        mobile_number=data['mobile_number'])
        db.session.add(new_user)
        db.session.commit()

        return jsonify({'message': 'New User is created', 'result': 'success',
                        'user_name': new_user.name, 'user_email': new_user.email_id,
                        'user_mobile_number': new_user.mobile_number, 'user_public_id': new_user.public_id}), 201


@app.route('/users/<public_id>/edit', methods=['PUT'])
def update_user(public_id):
    data = request.get_json()
    user = User.query.filter_by(public_id=public_id).first()
    if not user:
        return jsonify({'message': 'no user found', 'result': 'failure'}), 404
    duplicate_email_user = User.query.filter_by(email_id=data['email_id']).first()
    duplicate_mobile_number_user = User.query.filter_by(mobile_number=data['mobile_number']).first()
    if duplicate_email_user:
        if duplicate_email_user.public_id != user.public_id:
            return jsonify(
                {'message': 'User with email has already exisit', 'result': 'failure'}), 409
    if duplicate_mobile_number_user:
        if duplicate_mobile_number_user.public_id != user.public_id:
            return jsonify({'message': 'User with mobile number has already exisit', 'result': 'failure'}), 409
    user.name = data['name']
    user.email_id = data['email_id']
    user.mobile_number = data['mobile_number']
    db.session.commit()
    return jsonify({'message': 'user details is updated', 'result': 'success'})


@app.route('/users/<public_id>/delete', methods=['DELETE'])
def delete_user(public_id):
    user = User.query.filter_by(public_id=public_id).first()
    if not user:
        return jsonify({'message': 'no user found', 'result': 'failure'}), 404
    else:
        db.session.delete(user)
        db.session.commit()
        return jsonify({'message': 'User has been deleted', 'result': 'success'}), 200


if __name__ == "__main__":
    app.run(debug=True)
