
from main import app, db
import unittest
import json


class FlaskTestCase(unittest.TestCase):


    def setUp(self):
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        self.app = app.test_client()
        self.app.testing = True
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_index(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)

    def test_get_all_users(self):
        data = dict(name='akhil', email_id='akhil@gmail.com', mobile_number='8657412365')
        self.app.post('/users/create', data=json.dumps(data), content_type='application/json')
        response = self.app.get('/users')
        self.assertEqual(response.status_code, 200)
        users = json.loads(response.get_data(as_text=True))
        assert len(users)

    def test_add_user(self):
        data = dict(name='hari', email_id='hari@gmail.com', mobile_number='6657412365')
        response = self.app.post('/users/create', data=json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 201)

    def test_add_user_existing_email_id(self):
        data = dict(name='hari', email_id='hari@gmail.com', mobile_number='6657412365')
        response = self.app.post('/users/create', data=json.dumps(data), content_type='application/json')
        data = dict(name='hoii', email_id='hari@gmail.com', mobile_number='2857412365')
        response = self.app.post('/users/create', data=json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 409)

    def test_add_user_existing_mobile_number(self):
        data = dict(name='hari', email_id='hari@gmail.com', mobile_number='6657412365')
        response = self.app.post('/users/create', data=json.dumps(data), content_type='application/json')
        data = dict(name='hai', email_id='hl11@gmail.com', mobile_number='6657412365')
        response = self.app.post('/users/create', data=json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 409)

    def test_get_single_user(self):
        data = dict(name='bluu', email_id='bluu@gmail.com', mobile_number='005648563')
        response = self.app.post('/users/create', data=json.dumps(data), content_type='application/json')
        user = json.loads(response.get_data(as_text=True))
        response = self.app.get('/users/' + str(user.get('user_public_id')))
        self.assertEqual(response.status_code, 200)
        user = json.loads(response.get_data(as_text=True))
        user = user.get('user')
        assert user.get('name') == 'bluu'

    def test_get_not_exisiting_user(self):
        response = self.app.get('/users/a9kj39202832')
        self.assertEqual(response.status_code, 404)

    def test_delete_user(self):
        data = dict(name='hay3', email_id='hay3@gmail.com', mobile_number='3944244163')
        response = self.app.post('/users/create', data=json.dumps(data), content_type='application/json')
        user = json.loads(response.get_data(as_text=True))
        response = self.app.delete('/users/' + str(user.get('user_public_id')) + '/delete')
        self.assertEqual(response.status_code, 200)

    def test_delete_user_not_exist(self):
        response = self.app.delete('/users/9k3h62802jskf9933/delete')
        self.assertEqual(response.status_code, 404)

    def test_update_user(self):
        data = dict(name='Harry', email_id='Harry@gmail.com', mobile_number='9957412365')
        response = self.app.post('/users/create', data=json.dumps(data), content_type='application/json')
        user = json.loads(response.get_data(as_text=True))
        data = dict(name='Harry', email_id='Harry1@gmail.com', mobile_number='3657412365')
        response = self.app.put('/users/' + str(user.get('user_public_id')) + '/edit', data=json.dumps(data),
                                content_type='application/json')
        self.assertEqual(response.status_code, 200)

    def test_update_user_not_exist(self):
        data = dict(name='Harry', email_id='Harry1@gmail.com', mobile_number='3657412365')
        response = self.app.put('/users/u0j3j02i3ppm3p3/update', data=json.dumps(data),
                                content_type='application/json')
        self.assertEqual(response.status_code, 404)

    def test_update_user_existing_email_id(self):
        data = dict(name='Jithin', email_id='jithin@gmail.com', mobile_number='3657412365')
        response = self.app.post('/users/create', data=json.dumps(data), content_type='application/json')
        data = dict(name='Anooj', email_id='anooj@gmail.com', mobile_number='4657412365')
        response = self.app.post('/users/create', data=json.dumps(data), content_type='application/json')
        user = json.loads(response.get_data(as_text=True))
        data = dict(name='Anooj', email_id='jithin@gmail.com', mobile_number='3657412365')
        response = self.app.put('/users/' + str(user.get('user_public_id')) + '/edit', data=json.dumps(data),
                                content_type='application/json')
        self.assertEqual(response.status_code, 409)

    def test_update_user_existing_mobile_number(self):
        data = dict(name='Jithin', email_id='jithin@gmail.com', mobile_number='3657412365')
        response = self.app.post('/users/create', data=json.dumps(data), content_type='application/json')
        data = dict(name='Anooj', email_id='anooj@gmail.com', mobile_number='4657412365')
        response = self.app.post('/users/create', data=json.dumps(data), content_type='application/json')
        user = json.loads(response.get_data(as_text=True))
        data = dict(name='Anooj', email_id='anooj@gmail.com', mobile_number='3657412365')
        response = self.app.put('/users/' + str(user.get('user_public_id')) + '/edit', data=json.dumps(data),
                                content_type='application/json')
        self.assertEqual(response.status_code, 409)


if __name__ == '__main__':
    unittest.main()
